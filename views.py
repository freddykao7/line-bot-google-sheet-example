from django.http import HttpResponse, HttpRequest
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from rest_framework.decorators import api_view
from django.http.response import JsonResponse

# Import the Python Library of LINE official bot
# 載入Line官方機器人的Python套件
from linebot import (LineBotApi, WebhookHandler)
from linebot.exceptions import (InvalidSignatureError)
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage, TemplateSendMessage, 
    CarouselColumn, CarouselTemplate,
    MessageAction, URIAction, PostbackAction)

# Environment
# 取得環境變數套件
import environ
env = environ.Env(
    # set casting, default value
    DEBUG=(bool, False)
)

environ.Env.read_env()

# # Reads the configuration from .env file
# Gets LINE access keys
# 取得 LINE 存取的金鑰
CHANNEL_ACCESS_TOKEN = env('LINE_CHANNEL_ACCESS_TOKEN')
CHANNEL_SECRET = env('LINE_CHANNEL_SECRET')
CHANNEL_USER_ID = env('LINE_CHANNEL_USER_ID')

line_bot_api = LineBotApi(CHANNEL_ACCESS_TOKEN)
handler = WebhookHandler(CHANNEL_SECRET)


@api_view(['GET', 'POST'])
def drf_hello(request):
    if request.method == 'GET':
        return JsonResponse({'msg': 'Hello! GET method API!'})
    elif request.method == 'POST':
        return JsonResponse({'msg': 'Hello! POST method API!'})


@api_view(['POST'])
def drf_linebot_send_text(request):
    line_bot_api.push_message(CHANNEL_USER_ID, TextSendMessage(text='Send messages from Divio'))
    return JsonResponse({'msg': 'success'})


@api_view(['POST'])
def drf_linebot_send_carousel_template(request):
    carousel_template_message = TemplateSendMessage(
        alt_text='Carousel template(輪轉樣式)',
        template=CarouselTemplate(
            columns=[
                CarouselColumn(
                    thumbnail_image_url='https://images.chinatimes.com/newsphoto/2019-07-31/900/20190731003452.jpg',
                    title='羅東博愛醫院',
                    text='羅東博愛醫院是宜蘭縣規模最大的「區域教學醫院」、宜蘭縣唯一通過「重度級急救責任醫院」',
                    actions=[
                        URIAction(
                            label='官方網站',
                            uri='https://www.pohai.org.tw/register.php'
                        ),
                        URIAction(
                            label='Google地圖連結',
                            uri='https://goo.gl/maps/ckC4dnnUuLXLKxxJ9'
                        )
                    ]
                ),
                CarouselColumn(
                    thumbnail_image_url='https://photo.travelking.com.tw/scenery/2FA12285-638C-4240-B0B4-F713312DE498_e.jpg',
                    title='宜蘭運動公園',
                    text='蒸氣火車頭附近有提供一些公共孩童遊樂設施',
                    actions=[
                        URIAction(
                            label='官方網站',
                            uri='https://gym.e-land.gov.tw/cp.aspx?n=4DCB7D65F84E8242'
                        ),
                        URIAction(
                            label='Google地圖連結',
                            uri='https://goo.gl/maps/p4S5nHXSMq7qD7Fy9'
                        )
                    ]
                )
            ]
        )
    )

    line_bot_api.push_message(CHANNEL_USER_ID, carousel_template_message)
    return JsonResponse({'msg': 'success'})

    
@api_view(['POST'])
def drf_linebot_webhook_parrot(request):
    if request.method == 'POST':
        
        signature = request.headers["X-Line-Signature"]
        body_unicode = request.body.decode('utf-8')

        try:
            handler.handle(body_unicode, signature)
            line_bot_api.push_message(CHANNEL_USER_ID, TextSendMessage(text=body))
        except InvalidSignatureError:
            messages = (
                "Invalid signature. Please check your channel access token/channel secret."
            )
            return JsonResponse({'msg': messages})
        return JsonResponse({'msg': 'success'})


@handler.add(event=MessageEvent, message=TextMessage)
def handl_message(event):
    # Receive event and then reply message to the specific token instantly.
    # 即時取得訊息事件並且回覆訊息到指定的TOKEN
    
    # Parse the event
    # 解析事件內容
    msg = event.message.text
    msg = msg.encode('utf-8').decode('utf-8')

    # Reply messages
    # 回覆訊息
    line_bot_api.reply_message(
        reply_token=event.reply_token,
        messages=TextSendMessage(text=str(msg)),
    )

    
