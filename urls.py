# -*- coding: utf-8 -*-
from django.conf.urls import url, include
from aldryn_django.utils import i18n_patterns
import aldryn_addons.urls

import views
import views_gsheet

urlpatterns = [
    # ****************************************************************************
    # 簡單測試 DRF
    url(r'^api/hello$', views.drf_hello),
    # ****************************************************************************
    # 簡單測試 Google Sheet Cell
    url(r'^gsheet/sheet/get/cell_value$', views_gsheet.drf_gsheet_get_cell_value),
    url(r'^gsheet/qa$', views_gsheet.drf_gsheet_qa),
    url(r'^gsheet/tags$', views_gsheet.drf_gsheet_tags),
    url(r'^gsheet/qa/query$', views_gsheet.drf_gsheet_qas_by_tag),
    # ****************************************************************************
    # 使用DRF實做測試LineBot Python套件
    url(r'^linebot/send/text$', views.drf_linebot_send_text),
    url(r'^linebot/send/carousel$', views.drf_linebot_send_carousel_template),
    # 使用DRF實做一個LineBot WebHook來實做一個學人講話的鸚鵡App
    url(r'^linebot/webhook/parrot$', views.drf_linebot_webhook_parrot),
    # 使用DRF實做一個LineBot WebHook + Google Sheet 來實做一個Q&A的自動回覆機器人App
    url(r'^linebot/webhook/qa$', views_gsheet.drf_linebot_webhook_qa),
    
] + aldryn_addons.urls.patterns() + i18n_patterns(
    # Add your own i18n patterns here
    # 增加多國語系路徑
    *aldryn_addons.urls.i18n_patterns()  # MUST be the last entry!
)
